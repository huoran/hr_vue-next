import { defineConfig,UserConfigExport, ConfigEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import type { UserConfig } from 'vite';
import { viteMockServe } from 'vite-plugin-mock';
import { loadEnv } from './src/utils/viteBuild';

const { VITE_PORT, VITE_OPEN, VITE_PUBLIC_PATH } = loadEnv();

const pathResolve = (dir: string): any => {
    return resolve(__dirname, '.', dir);
};

const alias: Record<string, string> = {
    '/@': pathResolve('/src/'),
    'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js',
};

// https://vitejs.dev/config/
export default ({ command }: ConfigEnv): UserConfigExport => {
    return {
        plugins: [
            vue(),
            viteMockServe({
                // default
                mockPath: 'mock',
                localEnabled: command === 'serve',
                supportTs: true,
            })
        ],
        base: process.env.NODE_ENV === 'production' ? VITE_PUBLIC_PATH : './',
        resolve: {alias},
        root: process.cwd(),
        optimizeDeps: {
            include: ['element-plus/lib/locale/lang/zh-cn', 'element-plus/lib/locale/lang/en', 'element-plus/lib/locale/lang/zh-tw'],
        },
        server: {
            https: false, // 是否开启 https
            open: false, // 是否自动在浏览器打开
            port: 3000, // 端口号
            host: "0.0.0.0",
            proxy: {
            "/api": {
                target: "", // 后台接口
                changeOrigin: true,
                secure: false, // 如果是https接口，需要配置这个参数
                // ws: true, //websocket支持
                rewrite: (path) => path.replace(/^\/api/, ""),
                },
            },
        },
        build: {
            outDir: 'dist',
            minify: 'esbuild',
            sourcemap: false,
        },
        define: {
            __VUE_I18N_LEGACY_API__: JSON.stringify(false),
            __VUE_I18N_FULL_INSTALL__: JSON.stringify(false),
            __INTLIFY_PROD_DEVTOOLS__: JSON.stringify(false),
        },
    }
}
