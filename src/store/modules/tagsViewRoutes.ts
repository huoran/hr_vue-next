import { Module } from 'vuex';
import { ITagsViewRoutes, RootStateTypes } from '../interface/index';

const tagsViewRoutesModule: Module<ITagsViewRoutes, RootStateTypes> = {
    namespaced: true,
    state: {
        tagsViewRoutes: [],
    },
    mutations: {
        // 获取tagsView路由
        getTagsViewRoutes(state: any, data: Array<Object>) {
            state.tagsViewRoutes = data;
        },
    },
    actions: {
        // 设置tagsView路由
        setTagsViewRoutes({ commit }, data: Array<Object>) {
            commit('getTagsViewRoutes', data);
        },
    },
};

export default tagsViewRoutesModule;
