import { Module } from 'vuex';
import { IKeepAliveNames, RootStateTypes } from '../interface/index';

const keepAliveNamesModule: Module<IKeepAliveNames, RootStateTypes> = {
    namespaced: true,
    state: {
        keepAliveNames: [],
    },
    mutations: {
        // 获取keepAliveNames缓存
        getKeepAliveNames(state: any, data: Array<string>) {
            state.keepAliveNames = data;
        },
    },
    actions: {
        // 设置keepAliveNames缓存
        setKeepAliveNames({ commit }, data: Array<string>) {
            commit('getKeepAliveNames', data);
        },
    },
};

export default keepAliveNamesModule;
