import { Module } from 'vuex';
import { IThemeConfig, RootStateTypes } from '../interface/index';

const themeConfigModule: Module<IThemeConfig, RootStateTypes> = {
	namespaced: true,
	state: {
		themeConfig: {
			// 是否开启布局配置抽屉
			isDrawer: false,

			/**
			 * 全局主题
			 */
			// 默认 primary 颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			primary: '#409eff',
			// 默认 success 颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			success: '#67c23a',
			// 默认 info 颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			info: '#909399',
			// 默认 warning 颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			warning: '#e6a23c',
			// 默认 danger 颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			danger: '#f56c6c',

			/**
			 * 菜单 / 顶栏
			 * 注意：为了演示，切换布局时，颜色会被还原成默认，代码位置：/@/layout/navBars/breadcrumb/setings.vue
			 * 中的 `initSetLayoutChange(设置布局切换，重置主题样式)` 方法
			 */
			// 默认顶栏导航背景颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			topBar: '#ffffff',
			// 默认菜单导航背景颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			menuBar: '#001524',
			// 默认分栏菜单背景颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			columnsMenuBar: '#545c64',
			// 默认顶栏导航字体颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			topBarColor: '#606266',
			// 默认菜单导航字体颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			menuBarColor: '#eaeaea',
			// 默认分栏菜单字体颜色，请注意：需要同时修改 `/@/theme/common/var.scss` 对应的值
			columnsMenuBarColor: '#e6e6e6',

			/**
			 * 界面设置
			 */
			// 是否开启菜单水平折叠效果
			isCollapse: false,
			// header是否固定
			isFixedHeader: true,
			// 初始化变量，用于更新菜单 el-scrollbar 的高度，请勿删除
			isFixedHeaderChange: false,
			// 是否开启经典布局分割菜单（仅经典布局生效,左侧子菜单，顶部父菜单）
			isClassicSplitMenu: false,

			/**
			 * 界面显示
			 */
			// 是否显示侧边Logo
			isShowLogo: true,
			// 初始化变量，用于 el-scrollbar 的高度更新，请勿删除
			isShowLogoChange: false,
			// 是否开启 Breadcrumb
			isBreadcrumb: true,
			// 是否开启 Breadcrumb 图标
			isBreadcrumbIcon: true,
			// 是否开启 Tagsview
			isTagsview: true,
			// 是否开启 Tagsview 图标
			isTagsviewIcon: true,
			// 是否开启 TagsView 缓存, 刷新的时候，tagsView还在
			isCacheTagsView: true,
			// 是否开启 TagsView 拖拽
			isSortableTagsView: true,

			/**
			 * 布局切换
			 * 注意：为了演示，切换布局时，颜色会被还原成默认，代码位置：/@/layout/navBars/breadcrumb/setings.vue
			 * 中的 `initSetLayoutChange(设置布局切换，重置主题样式)` 方法
			 */
			// 布局切换：可选值"<defaults|classic|transverse|columns>"，默认 defaults
			layout: 'defaults',

			/**
			 * 其它设置
			 */
			// 主页面切换动画：可选值"<slide-right|slide-left|opacitys>"，默认 slide-right
			animation: 'slide-right',
			// Tagsview 风格：可选值"<tags-style-one|tags-style-two|tags-style-three|tags-style-four>"，默认 tags-style-one
			tagsStyle: 'tags-style-three',
			// 分栏高亮风格：可选值"<columns-round|columns-card>"，默认 columns-round
			columnsAsideStyle: 'columns-round',

			/**
			 * 后端控制路由
			 */
			// 是否开启后端控制路由
			isRequestRoutes: true,
		},
	},
	mutations: {
		// 设置布局配置
		getThemeConfig(state: any, data: object) {
			state.themeConfig = data;
		},
	},
	actions: {
		// 设置布局配置
		setThemeConfig({ commit }, data: object) {
			commit('getThemeConfig', data);
		},
	},
};

export default themeConfigModule;
