export interface RootStateTypes {
	user: IUserInfo;
	themeConfig: IThemeConfig;
	tagsViewRoutes: ITagsViewRoutes;
	keepAliveNames: IKeepAliveNames;
}

// 用户信息
export interface IUserInfo {
	token: string;
	name: string;
	avatar: string;
	userInfos: object;
	routerList: Array<object>;
}

// 布局配置
export interface IThemeConfig {
	themeConfig: {
		isDrawer: boolean;
		layout: string;
		primary: string;
		success: string;
		info: string;
		warning: string;
		danger: string;
		topBar: string;
		menuBar: string;
		columnsMenuBar: string;
		topBarColor: string;
		menuBarColor: string;
		columnsMenuBarColor: string;
		isCollapse: boolean;
		isRequestRoutes: boolean;
		isBreadcrumb: Boolean;
		isBreadcrumbIcon: boolean;
		isCacheTagsView: boolean;
		isTagsview: boolean;
		isTagsviewIcon: boolean;
		animation: string;
		isSortableTagsView: boolean;
		tagsStyle: string;
		isFixedHeader: Boolean;
		isFixedHeaderChange: Boolean;
		isShowLogo: Boolean;
		isShowLogoChange: Boolean;
		isClassicSplitMenu: Boolean;
		columnsAsideStyle: string;
	};
}

// TagsView 路由列表
export interface ITagsViewRoutes {
	tagsViewRoutes: Array<object>;
}

// 路由缓存列表
export interface IKeepAliveNames {
	keepAliveNames: Array<string>;
}
