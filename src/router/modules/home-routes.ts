import { RouteRecordRaw } from 'vue-router';

const homeRoutes: Array<RouteRecordRaw> = [
	{
		path: '/home',
		name: 'home',
		component: () => import('/@/views/home/index.vue'),
		meta: {
			title: '首页',
			auth: ['admin'],
			icon: '',
			isAffix: true,
			isKeepAlive: true,
		},
	},
];

export default homeRoutes;