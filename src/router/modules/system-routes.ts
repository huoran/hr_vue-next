import { RouteRecordRaw } from 'vue-router';

const systemRoutes: Array<RouteRecordRaw> = [
	{
		path: '/system',
		name: 'system',
		component: () => import('/@/layout/routerView/parent.vue'),
		redirect: '/system/menu',
		meta: {
			title: '系统配置',
			auth: ['admin'],
			icon: '',
			isAffix: false,
			isKeepAlive: true,
		},
		children: [
			{
				path: '/system/menu',
				name: 'systemMenu',
				component: () => import('/@/views/system/menu/index.vue'),
				meta: {
					title: '菜单管理',
					auth: ['admin'],
					isAffix: false,
					isKeepAlive: true,
				},
			},
			{
				path: '/system/user',
				name: 'systemUser',
				component: () => import('/@/views/system/user/index.vue'),
				meta: {
					title: '用户管理',
					auth: ['admin'],
					isAffix: false,
					isKeepAlive: true,
				},
			},
		],
	},
];

export default systemRoutes;
