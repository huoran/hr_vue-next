import homeRoutes from './modules/home-routes';
import systemRoutes from './modules/system-routes';

export default [...homeRoutes, ...systemRoutes];
