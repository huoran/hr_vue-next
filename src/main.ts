import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { store, key } from './store/index';

import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import '/@/theme/index.scss';
import './assets/font/font.css';
import './utils/rem';
import mitt from 'mitt';

const app = createApp(App);
app
	.use(router)
	.use(store, key)
	.use(ElementPlus)
	.mount('#app');

// 全局传值
app.config.globalProperties.mittBus = mitt();
