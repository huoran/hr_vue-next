import request from '/@/utils/request';

export interface LoginParam {
    userName: string;
    password: string;
}

export interface UserParam {
    user_id: string;
}

// 用户登录
export function loginApi(params: LoginParam) {
    return request({
        url: '/user/login',
        method: 'post',
        data: params,
    });
}

// 获取用户信息
export function getUserInfoApi(params: UserParam) {
    return request({
        url: '/user/getUserInfo',
        method: 'post',
        data: params,
    });
}

// 获取路由
export function getMenuListApi() {
    return request({
        url: '/user/getMenuList',
        method: 'post',
    });
}
