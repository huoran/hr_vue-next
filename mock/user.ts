import { MockMethod } from 'vite-plugin-mock';

const users = [
	{
		userName: 'admin',
		password: '123456',
		userId: 'admin',
		avatar:
			'https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/zhidao/wh%3D450%2C600/sign=0165a38bf91986184112e7807fdd0245/d000baa1cd11728bf56eaac8c9fcc3cec2fd2c9c.jpg',
		userInfos: {},
	},
	{
		userName: 'test',
		password: '123456',
		userId: 'test',
		avatar: '',
		userInfos: {},
	},
];

const menuList = [
	{
		path: '/home',
		name: 'home',
		meta: {
			auth: ['admin', 'test'],
			title: '首页',
			icon: 'icon-shouye_3',
			isAffix: true,
			isHide: false,
		},
	},
	{
		path: '/system',
		name: 'system',
		redirect: '/system/menu',
		meta: {
			auth: ['admin'],
			title: '系统配置',
			isHide: false,
			icon: 'icon-xitongpeizhi',
			isAffix: false,
		},
		children: [
			{
				path: '/system/menu',
				name: 'systemMenu',
				meta: {
					auth: ['admin'],
					title: '菜单管理',
					isHide: false,
					isAffix: false,
				},
			},
			{
				path: '/system/user',
				name: 'systemUser',
				meta: {
					auth: ['admin'],
					title: '用户管理',
					isHide: false,
					isAffix: false,
				},
			},
		],
	},
];

const mocks: MockMethod[] = [
	//
	{
		url: '/user/login',
		method: 'post',
		response: ({ body }) => {
			const { userName, password } = body;
			const res = users.filter((item) => {
				return item.userName == userName && item.password == password;
			});
			if (res.length > 0) {
				return {
					code: 0,
					data: {
						user_id: res[0].userId,
					},
					message: '登录成功！',
					token: 'token',
				};
			} else {
				return {
					code: 1,
					data: '',
					message: '账号或密码错误！',
					token: '',
				};
			}
		},
	},
	// 获取用户信息
	{
		url: '/user/getUserInfo',
		method: 'post',
		response: ({ body }) => {
			const { user_id } = body;
			const res = users.filter((item) => {
				return item.userId == user_id;
			});
			if (res.length > 0) {
				return {
					code: 0,
					data: res[0],
					message: '请求成功！',
					token: 'token',
				};
			} else {
				return {
					code: 1,
					data: '',
					message: '请求失败！',
					token: '',
				};
			}
		},
	},
	{
		url: '/user/getMenuList',
		method: 'post',
		response: ({ body }) => {
			const { userId } = body;

			return {
				code: 0,
				data: menuList,
				message: '请求成功！',
				token: 'token',
			};
		},
	},
];

// 后端控制路由
function backEndRouter(routes: any, userId: string) {
	if (!routes) return;
	return routes.filter((item: any) => {
		if (item.auth.indexOf(userId) > -1) {
			item.children && backEndRouter(item.children, userId);
			return item;
		}
	});
}

export default mocks;
